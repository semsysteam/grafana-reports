#!/usr/bin/env python3
'''
Created Date: Monday, June 28th 2021, 3:26:55 pm
Author: Sergey A. Kolesnikov

Copyright (c) 2021 Sergey A. Kolesnikov
'''
import os
from dotenv import load_dotenv
from grafana_api.grafana_face import GrafanaFace


class DataSourceCreator:
    def __init__(self) -> None:
        self.dotenv_path = os.path.join(os.path.dirname(
            os.path.realpath(__file__)), ".env")
        load_dotenv(self.dotenv_path)
        self.base_url = os.environ.get("GRAFANA_URL")
        self.port = os.environ.get("GRAFANA_PORT")
        self.login = os.environ.get("GRAFANA_USERNAME")
        self.password = os.environ.get("GRAFANA_PASSWORD")
        self.prometheus_ds_name = os.environ.get("PROMETHEUS_DS_NAME")
        self.prometheus_url_host = os.environ.get("PROMETHEUS_URL")
        self.prometheus_port = os.environ.get("PROMETHEUS_PORT")
        self.prometheus_login = os.environ.get("PROMETHEUS_USERNAME")
        self.prometheus_password = os.environ.get("PROMETHEUS_PASSWORD")

        def construct_api_url():
            params = {
                "host": self.prometheus_url_host
            }

            if self.prometheus_port is None:
                url_pattern = "{host}"
            else:
                params["port"] = self.prometheus_port
                url_pattern = "{host}:{port}"

            return url_pattern.format(**params)

        self.prometheus_url = construct_api_url()

    def main(self):
        try:
            if self.base_url != None and self.login != None and self.password != None and self.prometheus_url_host != None:
                ds_name = ""
                if self.prometheus_ds_name is None:
                    ds_name = "Prometheus"
                else:
                    ds_name = self.prometheus_ds_name

                grafana_api = GrafanaFace(
                    auth=(self.login, self.password), host=self.base_url, port=self.port, verify=False)
                ds = {'name': ds_name, 'type': "prometheus", 'access': "proxy", 'url': self.prometheus_url, 'password': "", 'user': "", 'basicAuth': False, 'basicAuthUser': self.prometheus_login,
                      'basicAuthPassword': self.prometheus_password, 'withCredentials': False, 'isDefault': True, 'jsonData': {'httpMethod': "POST", 'tlsSkipVerify': True}, 'secureJsonFields': {}, 'version': 5, 'readOnly': False}
                try:
                    grafana_api.datasource.create_datasource(datasource=ds)
                except:
                    raise
            else:
                raise Exception("В файле " + self.dotenv_path +
                                " одна или несколько переменных не заполнены!")

        except Exception as e:
            print(e)
            raise


dsc = DataSourceCreator()
dsc.main()
