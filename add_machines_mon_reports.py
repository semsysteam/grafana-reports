#!/usr/bin/env python3
'''
Created Date: Tuesday, June 29th 2021, 3:00:38 pm
Author: Sergey A. Kolesnikov

Copyright (c) 2021
'''
import os
import json
from dotenv import load_dotenv
from grafana_api.grafana_face import GrafanaFace


class AddMachinesMonReports:
    def __init__(self):
        self.dotenv_path = os.path.join(os.path.dirname(
            os.path.realpath(__file__)), ".env")
        load_dotenv(self.dotenv_path)
        self.base_host = os.environ.get("GRAFANA_HOST")
        self.port = os.environ.get("GRAFANA_PORT")
        self.login = os.environ.get("GRAFANA_USERNAME")
        self.password = os.environ.get("GRAFANA_PASSWORD")
        self.dashboards_path = os.path.join(os.path.dirname(
            os.path.realpath(__file__)), "dashboards")

    def main(self):
        try:
            if self.base_host != None and self.login != None and self.password != None:
                grafana_api = GrafanaFace(
                    auth=(self.login, self.password), host=self.base_host, port=self.port, verify=False)
                try:
                    if not os.path.exists(self.dashboards_path):
                        raise Exception("Каталог с отчетами не найден!")
                    else:
                        dashboards = []
                        for _, _, files in os.walk(self.dashboards_path, topdown=False):
                            for file_name in files:
                                if file_name.endswith('.json'):
                                    file_path = os.path.join(os.path.realpath(
                                        self.dashboards_path), file_name)
                                    with open(file=file_path, encoding='utf-8') as json_file:
                                        d_obj = json.load(json_file)
                                        if "overwrite" in d_obj:
                                            overwrite = d_obj['overwrite']
                                            if overwrite is False:
                                                d_obj['overwrite'] = True
                                            print(d_obj)
                                        dashboards.append(d_obj)

                        for dashboard in dashboards:
                            try:
                                resp = grafana_api.dashboard.update_dashboard(
                                    dashboard)
                                print(resp)
                            except Exception as exc:
                                print(exc)
                except:
                    raise
            else:
                raise Exception("В файле " + self.dotenv_path +
                                " одна или несколько переменных не заполнены!")

        except Exception as exc:
            raise exc


m = AddMachinesMonReports()
m.main()
