#!/usr/bin/env bash
#
# Created Date: Tuesday, June 29th 2021, 2:57:43 pm
# Author: Sergey A. Kolesnikov
#
# Copyright (c) 2021
#
set -euo pipefail

target="add_machines_mon_reports.py"

dir=$(pwd)

if [[ $EUID = 0 ]]; then
    printf "ВНИМАНИЕ! Не запускайте этот скрипт пользователем root"
    exit 1
fi

command_exists() {
    command -v "$@" >/dev/null 2>&1
}

print_target_missing() {
    clear
    echo "  ###                      ###"
    echo " #     Файл Не Найден     #"
    echo "###                      ###"
    echo
    echo "Файл $target не найден!"
    echo
    exit 1
}

_main() {
    clear
    if [ ! -f "$dir/$target" ]; then
        print_target_missing
    fi
    if ! command_exists python3; then
        sudo apt-get update -qq >/dev/null
        sudo apt-get -y -qq install python3
    fi
    if ! command_exists pip3; then
        sudo apt-get update -qq >/dev/null
        sudo apt-get -y -qq install python3-pip
    fi
    pip3 install -U pyjson python-dotenv grafana_api
    python3 "$dir/$target"
}

_main
