#!/usr/bin/env bash
#
# Created Date: Monday, June 28th 2021, 3:23:58 pm
# Author: Sergey A. Kolesnikov
#
# Copyright (c) 2021 Sergey A. Kolesnikov
#

set -euo pipefail

target="datasource_creator.py"

dir=$(pwd)

if [[ $EUID = 0 ]]; then
    printf "ВНИМАНИЕ! Не запускайте этот скрипт пользователем root"
    exit 1
fi

command_exists() {
    command -v "$@" >/dev/null 2>&1
}
print_target_missing() {
    clear
    echo "  ###                      ###"
    echo " #     Файл Не Найден     #"
    echo "###                      ###"
    echo
    echo "Файл $target не найден!"
    echo
    exit 1
}
_main() {
    clear
    if [ ! -f "$dir/$target" ]; then
        print_target_missing
    fi
    if ! command_exists python3; then
        sudo apt-get update -qq >/dev/null
        sudo apt-get -y -qq install python3
    fi
    if ! command_exists pip3; then
        sudo apt-get update -qq >/dev/null
        sudo apt-get -y -qq install python3-pip
    fi
    pip3 install -U pyjson python-dotenv grafana_api
    python3 "$dir/$target"
}

_main
